# Fujitsu Lifebook E744 Hackintosh

Repository containing all necessary files in order to run perfectly macOS on the Lifebook E744

## Hardware Configuration

- i5 4300M
- 8GB Ram Single Slot
- Sandisk 256GB SSD
- Intel Ethernet I217-V
- Dell DW1560 Wifi + Bluetooth
- Realtek SD Card Reader
- Realtek ALC282 (ALC 3233)
- 1600x900 14" Display
- Display Port

## Preparation

Access bios and disable secureboot & TMP, set sata mode to AHCI and disable VT-d.

Prepare a usb drive with Mojave and copy the clover folder from the bootpack in this repo into the efi folder in the partition of the usb drive.

## Installation

Install macOS normally, then move the clover folder to the efi partition in your hard disk, remove from that clover folder the kext except for VirtualSMC, and install the kext from postinstall to Library/Extentions

### What Works

Pretty much everything, including sleep and all gestures (except 4 fingers)

### What doesn't work

- Fingerprint Sensor (Disabled via Bios)
- WWAN Card (Disabled via bios)


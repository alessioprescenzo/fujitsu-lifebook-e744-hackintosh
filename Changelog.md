## Changelog


Version 2.0.0: Catalina + OpenCore

* Migrated from Clover to OpenCore
* Updated README.md and moved clover's instruction to OLD_Readme.md
* Updated all kexts

Version 1.1.0: Catalina

* Migrated to VirtualSMC and SMCBatteryManager
* Fixed HDMI Audio Kernel panic with Catalina
* Upgraded to 16Gb Ram (Clover.plist Memory Module configuration might change for you)
* Migrated to VoodooPS2Controller + VoodooInput from acidanthera
* Updated all kexts to the latest available

Version 1.0.0: Mojave 

* Initial bootpack and postinstall package for Mojave

​	

